"use strict";

//  Теоретичні питання
//  1. Як можна сторити функцію та як ми можемо її викликати?
// Функцію можна створити за допомогою ключового слова `function`. Вона може приймати параметри, виконувати певні дії та повертати значення.

function sayHello() {
  console.log("Hello world!");
}
sayHello(); // Виклик функції за допомогою ім`я функції, круглих дужок та крапки з комою.

//  2. Що таке оператор return в JavaScript? Як його використовувати в функціях?

// В JavaScript оператор return використовується в тілі функції для повернення значення з функції. Коли викликається оператор return, виконання функції припиняється, і значення, яке вказано після return, передається назад до коду, який викликав функцію.

function add(a, b) {
  return a + b;
}
let result = add(3, 5);
console.log(result);

//  якщо в функції відсутній оператор return або якщо він вказаний без значення, то функція поверне undefined.

//  3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?

// Параметри - це змінні, які вказуються в оголошенні функції, розміщаються в круглих дужках при оголошенні функції.

function multiply(a, b) {
  // `a` та `b` - це параметри
  return a * b;
}
// Аргументи - це конкретні значення, які передаються функції при її виклику.  вказуються в дужках при виклику функції.

let result2 = multiply(3, 4);
console.log(result2);

//  4. Як передати функцію аргументом в іншу функцію?

//  Ми можемо передати іншу функцію як аргумент в функцію за допомогою `callback`.

function sayHello() {
  console.log("Привіт!");
}
function doSomething(callback) {
  callback();
}

doSomething(sayHello);

//  Практичні завдання
//  1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.

function remainderNumber(a, b) {
  if (b !== 0) {
    return a / b;
  } else {
    console.log("На нуль ділення неможливе.");
  }
}
let result3 = remainderNumber(10, 2);
console.log(result3);

result3 = remainderNumber(10, 0);

//  2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа). Якщо користувач ввів не число, запитувати до тих пір, поки не введе число
// - Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /. Провалідувати отримане значення. Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').
// - Створити функцію, в яку передати два значення та операцію.
// - Вивести у консоль результат виконання функції.

let firstNumber, secondNumber;
let mathOperation;
let result4;

function simplCalc() {
  while (true) {
    firstNumber = prompt(`Введіть перше число`);
    if (firstNumber === null) {
      alert("Ви відмінили введення. Спробуйте ще раз.");
      continue;
    } else if (firstNumber === `` || isNaN(firstNumber)) {
      alert("Введене не коректне значення.");
      continue;
    }
    break;
  }
  while (true) {
    secondNumber = prompt(`Введіть друге число`);
    if (secondNumber === null) {
      alert("Ви відмінили введення. Спробуйте ще раз.");
      continue;
    } else if (secondNumber === `` || isNaN(secondNumber)) {
      alert("Введене не коректне значення.");
      continue;
    }
    break;
  }
  firstNumber = +firstNumber;
  secondNumber = +secondNumber;
  console.log(`Перше число:`, firstNumber);
  console.log(`Друге число:`, secondNumber);
  while (true) {
    mathOperation = prompt(
      `Введіть математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.`
    );
    if (mathOperation === null) {
      alert("Ви відмінили введення. Спробуйте ще раз.");
    } else if (
      mathOperation !== `+` &&
      mathOperation !== `-` &&
      mathOperation !== `*` &&
      mathOperation !== `/`
    ) {
      alert("Такої операції не існує");
      continue;
    } else {
      break;
    }
  }
  console.log(`Математична операція:`, mathOperation);
  switch (mathOperation) {
    case `+`:
      result4 = firstNumber + secondNumber;
      break;
    case `-`:
      result4 = firstNumber - secondNumber;
      break;

    case `/`:
      result4 = firstNumber / secondNumber;
      break;
    case `*`:
      result4 = firstNumber * secondNumber;
      break;
  }
  console.log(
    `Над числами ${firstNumber} і ${secondNumber} було проведено операцію ${mathOperation}. Результат: ${result4}`
  );
}
simplCalc();

// 3. Опціонально. Завдання:
// Реалізувати функцію підрахунку факторіалу числа.
// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера число, яке введе користувач.
// - За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
// - Використовувати синтаксис ES6 для роботи зі змінними та функціями.

function factorialNum() {
  let userNumber;
  let result5 = 1;
  while (true) {
    userNumber = prompt(`Введіть число, фактиоріал якого треба вирахувать.`);
    if (userNumber === null) {
      alert("Ви відмінили введення. Спробуйте ще раз.");
      continue;
    } else if (userNumber === `` || isNaN(userNumber)) {
      alert("Введене не коректне значення.");
      continue;
    }
    break;
  }
  for (let і = 1; і <= userNumber; ++і) {
    result5 = result5 * і;
  }
  userNumber = +userNumber;
  result5 = +result5;
  console.log(
    `Прораховано факторіал числа: ${userNumber}. Результат: ${result5}`
  );
}
factorialNum();
